#![crate_type="lib"]

#![macro_use]
extern crate diesel;
extern crate r2d2;
extern crate r2d2_diesel;


pub mod connection;


#[macro_export]
macro_rules! load {
    ($table: ident) => {
        pub fn load(pool: &pool::Pool<MysqlConnection>) -> anyhow::Result<Vec<Self>> {
            Ok($table::table
                .load::<Self>(&*pool.get()?)?)
        }

        pub fn load_per_limit(pool: &pool::Pool<MysqlConnection>, n: i64) -> anyhow::Result<Vec<Self>> {
            Ok(diesel::QueryDsl::limit($table::table, n)
                .load::<Self>(&*pool.get()?)?)
        }
    };
}

#[macro_export]
macro_rules! insert {
    ($table: ident, $new: ident) => {
        pub fn insert(pool: &pool::Pool<MysqlConnection>, new: $new) -> anyhow::Result<usize> {
            Ok(diesel::insert_into($table::table)
                .values(new)
                .execute(&*pool.get()?)?)
        }
    };
}

#[macro_export]
macro_rules! update {
    ($table: ident, $update: ty) => {
        pub fn update(pool: &pool::Pool<MysqlConnection>, update: $update) -> anyhow::Result<usize> {
            Ok(diesel::update($table::table)
                .set(update)
                .execute(&*pool.get()?)?)
        }
    };

    ($table: ident, $update: ty, $fn: ident, $($col: ident as $type: ty), +) => {
        pub fn $fn(pool: &pool::Pool<MysqlConnection>, update: $update, $($col: $type), +) -> anyhow::Result<usize> {
            Ok(diesel::update($table::table)
                $(.filter($table::$col.eq($col)))+
                .set(update)
                .execute(&*pool.get()?)?)
        }
    };
}

#[macro_export]
macro_rules! delete {
    ($table: ident, $fn: ident, $($col: ident as $type: ty), +) => {
        pub fn $fn(pool: &pool::Pool<MysqlConnection>, $($col: $type), +) -> anyhow::Result<usize> {
            Ok(diesel::delete($table::table)
                $(.filter($table::$col.eq($col)))+
                .execute(&*pool.get()?)?)
        }
    };
}

#[macro_export]
macro_rules! select {
    ($table: ident, $fn: ident, $($col: ident as $type: ty), +) => {
        pub fn $fn(pool: &pool::Pool<MysqlConnection>, $($col: $type), +) -> anyhow::Result<Vec<Self>> {
            Ok($table::table
                $(.filter($table::$col.eq($col)))+
                .load::<Self>(&*pool.get()?)?)
        }
    };
}
