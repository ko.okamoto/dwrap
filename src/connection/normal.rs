
use diesel::prelude::*;
use diesel::mysql::MysqlConnection;



pub fn establish_connection(database_url: String) -> Result<MysqlConnection, diesel::ConnectionError> {
    MysqlConnection::establish(&database_url)
}
