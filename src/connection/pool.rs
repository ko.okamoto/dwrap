
use diesel::mysql::MysqlConnection;
use r2d2_diesel::ConnectionManager;



pub type Pool<A> = r2d2::Pool<ConnectionManager<A>>;
pub type Pooled<A> = r2d2::PooledConnection<ConnectionManager<A>>;


pub fn connection_pooling(database_url: String) -> Result<Pool<MysqlConnection>, r2d2::Error> {
    let manager = ConnectionManager::<MysqlConnection>::new(&database_url);
    Pool::builder().max_size(15).build(manager)
}

pub struct Context {
    pub pool: Pool<MysqlConnection>,
}
